#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD || !process.env.EMAIL) {
    console.log('USERNAME, PASSWORD and EMAIL env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const SSH_PORT = 29420;

    let app, browser;

    const repodir = '/tmp/testrepo';
    const reponame = 'testrepo';

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
        fs.rmSync(repodir, { recursive: true, force: true });
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function setAvatar() {
        await browser.get('https://' + app.fqdn + '/user/settings');

        var button = await browser.findElement(By.xpath('//label[contains(text(), "Use Custom Avatar")]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.findElement(By.xpath('//label[contains(text(), "Use Custom Avatar")]')).click();
        await browser.findElement(By.xpath('//input[@type="file" and @name="avatar"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        await browser.findElement(By.xpath('//button[contains(text(), "Update Avatar")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(),"Your avatar has been updated.")]')), TIMEOUT);
    }

    async function checkAvatar() {
        await browser.get(`https://${app.fqdn}/${username}`);

        const avatarSrc = await browser.findElement(By.xpath('//div[@id="profile-avatar"]/a/img')).getAttribute('src');

        const response = await superagent.get(avatarSrc);
        expect(response.statusCode).to.equal(200);
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn + '/user/login');

        await browser.findElement(By.id('user_name')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//form[@action="/user/login"]//button')).click();
        await browser.wait(until.elementLocated(By.xpath('//img[contains(@class, "avatar")]')), TIMEOUT);
    }

    async function adminLogin() {
        await login('root', 'changeme');
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/user/login`);
        await browser.sleep(2000);


        await browser.findElement(By.xpath('//a[contains(@class, "openidConnect") and contains(., "Sign in with cloudron")]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//img[contains(@class, "avatar")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);

        await browser.findElement(By.xpath('//img[contains(@class, "avatar")]')).click();
        await sleep(2000);
        await browser.findElement(By.xpath('//a[@data-url="/user/logout"]')).click();
        await sleep(2000);
    }

    async function addPublicKey() {
        const publicKey = fs.readFileSync(__dirname + '/id_ed25519.pub', 'utf8');
        execSync(`chmod g-rw,o-rw ${__dirname}/id_ed25519`); // ssh will complain about perms later

        await browser.get('https://' + app.fqdn + '/user/settings/keys');

        await browser.wait(until.elementLocated(By.id('add-ssh-button')), TIMEOUT);
        await browser.findElement(By.id('add-ssh-button')).click();
        await browser.findElement(By.id('ssh-key-title')).sendKeys('testkey');
        await browser.findElement(By.id('ssh-key-content')).sendKeys(publicKey.trim()); // #3480
        var button = browser.findElement(By.xpath('//button[contains(text(), "Add Key")]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.findElement(By.xpath('//form//button[contains(text(),"Add Key")]')).click();

        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(), "has been added.")]')), TIMEOUT);
    }

    async function createRepo() {
        await browser.get(`https://${app.fqdn}/repo/create`);
        await browser.wait(until.elementLocated(By.id('repo_name')));
        await browser.findElement(By.id('repo_name')).sendKeys(reponame);
        var button = browser.findElement(By.xpath('//button[contains(text(), "Create Repository")]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.findElement(By.id('auto-init')).click();
        await browser.findElement(By.xpath('//button[contains(text(), "Create Repository")]')).click();

        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === 'https://' + app.fqdn + '/' + username + '/' + reponame;
            });
        }, TIMEOUT);
    }

    function cloneRepo() {
        fs.rmSync(repodir, { recursive: true, force: true });
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync(`git clone ssh://git@${app.fqdn}:${SSH_PORT}/${username}/${reponame}.git ${repodir}`, { env: env });
    }

    function pushFile() {
        const env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync(`touch newfile && git add newfile && git commit -a -mx && git push ssh://git@${app.fqdn}:${SSH_PORT}/${username}/${reponame} main`,
                 { env: env, cwd: repodir });
        fs.rmSync(repodir, { recursive: true, force: true });
    }

    function fileExists() {
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
    }

    async function sendMail() {
        await browser.get(`https://${app.fqdn}/-/admin/config`);
        await browser.sleep(3000);
        const button = await browser.findElement(By.xpath('//button[contains(., "Send")]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys('test@cloudron.io');
        await browser.findElement(By.xpath('//button[contains(., "Send")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//p[contains(., "A testing email has been sent")]')), TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION} -p SSH_PORT=${SSH_PORT}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', adminLogin);
    it('can send mail', sendMail);
    it('can logout', logout);

    it('can login', loginOIDC.bind(null, username, password, false));
    it('can set avatar', setAvatar);
    it('can get avatar', checkAvatar);

    it('can add public key', addPublicKey);

    it('can create repo', createRepo);

    it('can clone the url', cloneRepo);

    it('can add and push a file', pushFile);

    it('can restart app', function () { execSync('cloudron restart  --app ' + app.id); });

    xit('can login', loginOIDC.bind(null, username, password)); // no need to relogin since session persists
    it('can clone the url', cloneRepo);
    it('file exists in repo', fileExists);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () { execSync('cloudron restore --app ' + app.id, EXEC_ARGS); });

    it('can login', loginOIDC.bind(null, username, password));
    it('can get avatar', checkAvatar);
    it('can clone the url', cloneRepo);
    it('file exists in repo', function () { expect(fs.existsSync(repodir + '/newfile')).to.be(true); });

    it('move to different location', async function () {
        //browser.manage().deleteAllCookies(); // commented because of error "'Network.deleteCookie' wasn't found"
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');

        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can login', loginOIDC.bind(null, username, password));
    it('can get avatar', checkAvatar);
    it('can clone the url', cloneRepo);
    it('file exists in repo', function () { expect(fs.existsSync(repodir + '/newfile')).to.be(true); });

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // No SSO
    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION} -p SSH_PORT=${SSH_PORT}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login (no sso)', adminLogin);
    it('can logout', logout);

    it('uninstall app (no sso)', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION} -p SSH_PORT=${SSH_PORT}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', loginOIDC.bind(null, username, password));
    it('can set avatar', setAvatar);
    it('can get avatar', checkAvatar);
    it('can add public key', addPublicKey);
    it('can create repo', createRepo);
    it('can clone the url', cloneRepo);
    it('can add and push a file', pushFile);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', adminLogin);
    it('can send mail', sendMail);
    it('can logout', logout);

    it('can login', loginOIDC.bind(null, username, password));
    it('can get avatar', checkAvatar);
    it('can clone the url', cloneRepo);
    it('file exists in cloned repo', fileExists);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
