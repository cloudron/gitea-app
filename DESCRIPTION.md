Gitea is a painless self-hosted Git service. It is similar to GitHub, Bitbucket or Gitlab.

### Purpose

The goal of this project is to make the easiest, fastest, and most painless way to set up a self-hosted Git service.

### Features

- Activity timeline
- SSH and HTTP/HTTPS protocols
- SMTP/LDAP/Reverse proxy authentication
- Reverse proxy with sub-path
- Account/Organization/Repository management
- Repository/Organization webhooks (including Slack)
- Repository Git hooks/deploy keys
- Repository issues, pull requests and wiki
- Add/Remove repository collaborators
- Gravatar and custom source
- Mail service
- Administration panel

### Bug reports

Open bugs on [Github](https://git.cloudron.io/cloudron/gitea-app/issues)
