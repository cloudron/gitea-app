[0.1.0]
* Initial package (forked from Gogs app)

[0.1.1]
* Removed reference to Gogs

[0.1.2]
* Updated description

[0.1.3]
* Updated to version 1.1.2

[1.0.0]
* Update to version 1.1.3

[1.0.1]
* Update Git to v2.7.4-0ubuntu1.2
* Fixes critical security issue that allows remote command execution in git
* https://people.canonical.com/~ubuntu-security/cve/2017/CVE-2017-1000117.html

[1.0.2]
* Preserve SECRET_KEY across updates and restarts

[1.0.3]
* Update to version 1.1.4

[1.1.0]
* Update to version 1.2.0
* New logo!
* SECURITY: Sanitation fix from Gogs (#1461)
* Status-API
* Implement GPG api
* https://github.com/go-gitea/gitea/releases/tag/v1.2.0

[1.1.1]
* Update to version 1.2.1
* Fix PR, milestone and label functionality if issue unit is disabled (#2710) (#2714)
* Fix plain readme didn't render correctly on repo home page (#2705) (#2712)
* Fix so that user can still fork his own repository to his organizations (#2699) (#2707)
* Fix .netrc authentication (#2700) (#2708)
* Fix slice out of bounds error in mailer (#2479) (#2696)

[1.1.2]
* Update to version 1.2.2
* Add checks for commits with missing author and time (#2771) (#2785)
* Fix sending mail with a non-latin display name (#2559) (#2783)
* Sync MaxGitDiffLineCharacters with conf/app.ini (#2779) (#2780)
* Update vendor git (#2765) (#2772)
* Fix emojify image URL (#2769) (#2773)

[1.1.3]
* Update to version 1.2.3
* Only require one email when validating GPG key (#2266, #2467, #2663) (#2788)
* Fix order of comments (#2835) (#2839)

[1.2.0]
* Update to version 1.3.0

[1.3.0]
* Update to version 1.3.1
* Add documentationUrl
* Sanitize logs for mirror sync (#3057, #3082) (#3078)
* Fix missing branch in release bug (#3108) (#3117)
* Fix repo indexer and submodule bug (#3107) (#3110)
* Fix legacy URL redirects (#3100) (#3106)
* Fix redis session failed (#3086) (#3089)
* Fix issue list branch link broken (#3061) (#3070)
* Fix missing password length check when change password (#3039) (#3071)

[1.3.1]
* Update Gitea to 1.3.2
* Fix run web with -p push failed (#3154) (#3179)
* Fix source download link when no code unit allowed (#3166) (#3169)
* Allow adding collaborators with (fullname) (#3103) (#3168)
* Fix repo links (#3093) (#3163)
* Fix Uninitialized variable in ParsePatch (#3156) (#3162)
* Fix migration order v1.3 (#3157)
* Fix avatar URLs (#3069) (#3143)

[1.4.0]
* Fix email sending (use SMTPS)

[1.4.1]
* Update Gitea to 1.3.3
* Security fixes
    * Fix escaping changed title in comments (#3530) (#3535)
    * Escape search query display (#3486) (#3489)
* Bug fixes
    * Fix repo-transfer-and-team-repo-count bug (#3241) (#3244)
    * Open external tracker in blank window, consistently with wiki (#3227) (#3228)
    * Change SSL Mode from checkbox to string in admin page (#3208) (#3211)

[1.5.0]
* Update Gitea to 1.4.0

[1.5.1]
* Update Gitea to 1.4.1
* Add “error” as reserved username (#3882) (#3886)
* Do not allow inactive users to access repositories using private key (#3887) (#3889)
* Fix path cleanup in file editor, when initilizing new repository and LFS oids (#3871) (#3873)
* Remove unnecessary allowed safe HTML (#3778) (#3779)
* Correctly check http git access rights for reverse proxy authorized users (#3721) (#3743)
* Fix to use only needed columns from tables to get repository git paths (#3870) (#3883)
* Fix GPG expire time display when time is zero (#3584) (#3884)
* Fix to update only issue last update time when adding a comment (#3855) (#3860)
* Fix repository star count after deleting user (#3781) (#3783)
* Use the active branch for the code tab (#3720) (#3776)
* Set default branch name on first push (#3715) (#3723)
* Show clipboard button if disable HTTP of git protocol (#3773) (#3774)

[1.5.2]
* Update Gitea to 1.4.2
* Adjust z-index for floating labels (#3939) (#3950)
* Add missing token validation on application settings page (#3976) #3978
* Webhook and hook_task clean up (#4006)
* Fix webhook bug of response info is not displayed in UI (#4023)
* Fix writer cannot read bare repo guide (#4033) (#4039)
* Don't force due date to current time (#3830) (#4057)
* Fix wiki redirects (#3919) (#4065)
* Fix attachment ENABLED (#4064) (#4066)
* Added deletion of an empty line at the end of file (#4054) (#4074)
* Use ResolveReference instead of path.Join (#4073)
* Fix #4081 Check for leading / in base before removing it (#4083)
* Respository's home page not updated after first push (#4075)

[1.5.2-1]
* Rebuild Gitea package because of https://github.com/go-gitea/gitea/issues/4167
* Adjust z-index for floating labels (#3939) (#3950)
* Add missing token validation on application settings page (#3976) #3978
* Webhook and hook_task clean up (#4006)
* Fix webhook bug of response info is not displayed in UI (#4023)
* Fix writer cannot read bare repo guide (#4033) (#4039)
* Don't force due date to current time (#3830) (#4057)
* Fix wiki redirects (#3919) (#4065)
* Fix attachment ENABLED (#4064) (#4066)
* Added deletion of an empty line at the end of file (#4054) (#4074)
* Use ResolveReference instead of path.Join (#4073)
* Fix #4081 Check for leading / in base before removing it (#4083)
* Respository's home page not updated after first push (#4075)

[1.5.3]
* Update Gitea to 1.4.3
* SECURITY
  * HTML-escape plain-text READMEs (#4192) (#4214)
  * Fix open redirect vulnerability on login screen (#4312) (#4312)
* BUGFIXES
  * Fix broken monitoring page when running processes are shown (#4203) (#4208)
  * Fix delete comment bug (#4216) (#4228)
  * Delete reactions added to issues and comments when deleting repository (#4232) (#4237)
  * Fix wiki URL encoding bug (#4091) (#4254)
  * Fix code tab link when viewing tags (#3908) (#4263)
  * Fix webhook type conflation (#4285) (#4285)

[1.5.4]
* Allow customization using gitea's custom data directory

[1.6.0]
* Update Gitea to 1.5.0
* Security
  * Check that repositories can only be migrated to own user or organizations (#4366) (#4370)
  * Limit uploaded avatar image-size to 4096px x 3072px by default (#4353)
  * Do not allow to reuse TOTP passcode (#3878)
* Features
  * Add cli commands to regen hooks & keys (#3979)
  * Add support for FIDO U2F (#3971)
  * Added user language setting (#3875)
  * Add topic support (#3711)
  * Multiple assignees (#3705)
  * Add protected branch whitelists for merging (#3689)
  * Global code search support (#3664)
  * Add label descriptions (#3662)
  * Add issue search via API (#3612)
  * Add repository setting to enable/disable health checks (#3607)
  * Emoji Autocomplete (#3433)
  * Implements generator cli for secrets (#3531)

[1.6.1]
* Update Gitea to 1.5.1
* Security
  * Don't disclose emails of all users when sending out emails (#4784)
  * Improve URL validation for external wiki and external issues (#4710) (#4740)
  * Make cookies HttpOnly and obey COOKIE_SECURE flag (#4706) (#4707)
* Bugfixes
  * Fix missing release title in webhook (#4783) (#4800)
  * Make sure to reset commit count in the cache on mirror syncing (#4770)
  * Fixed bug where team with admin privelege type doesn't get any unit (#4759)
  * Fix failure on creating pull request with assignees (#4583) (#4727)
  * Hide org/create menu item in Dashboard if user has no rights (#4678) (#4686)

[1.7.0]
* Update base image

[1.7.1]
* Update Gitea to 1.5.2

[1.7.2]
* Update Gitea to 1.5.3
* Security
  * Fix remote command execution vulnerability in upstream library (#5177) (#5196)

[1.8.0]
* Update Gitea to 1.6.0

[1.8.1]
* Update Gitea to 1.6.1

[1.8.2]
* Update Gitea to 1.6.2
* SECURITY
  * Sanitize uploaded file names (#5571) (#5573)
  * HTMLEncode user added text (#5570) (#5575)
* BUGFIXES
  * Fix indexer reindex bug when gitea restart (#5563) (#5564)
  * Fix bug when a read perm user to edit his issue (#5516) (#5534)
  * Detect force push failure on deletion of protected branches (#5522) (#5531)
  * Fix forgot deletion of notification when delete repository (#5506) (#5514)
  * Fix undeleted content when deleting user (#5429) (#5509)
  * Fix empty wiki (#5504) (#5508)

[1.8.3]
* Update Gitea to 1.6.3
* SECURITY: Prevent DeleteFilePost doing arbitrary deletion (#5631)
* BUGFIX: Fix wrong text getting saved on editing second comment on an issue (#5608)

[1.8.4]
* Update Gitea to 1.6.4
* Fix SSH key now can be reused as public key after deleting as deploy key (#5671) (#5685)
* When redirecting clean the path to avoid redirecting to external site (#5669) (#5703)
* Fix to use correct value for "MSpan Structures Obtained" (#5706) (#5715)

[1.9.0]
* Update Gitea to 1.7.0

[1.9.1]
* Update Gitea to 1.7.1
* [Changelog](https://github.com/go-gitea/gitea/releases/tag/v1.7.1)
* Disable redirect for i18n (#5910) (#5916)
* Only allow local login if password is non-empty (#5906) (#5908)
* Fix go-get URL generation (#5905) (#5907)
* Fix TLS errors when using acme/autocert for local connections (#5820) (#5826)
* Request for public keys only if LDAP attribute is set (#5816) (#5819)
* Fix delete correct temp directory (#5840) (#5839)
* Fix an error while adding a dependency via UI (#5862) (#5876)
* Fix null pointer in attempt to Sudo if not logged in (#5872) (#5884)
* When creating new repository fsck option should be enabled (#5817) (#5885)
* Prevent nil dereference in mailIssueCommentToParticipants (#5891) (#5895) (#5894)
* Fix bug when read public repo lfs file (#5913) (#5912)
* Respect value of REQUIRE_SIGNIN_VIEW (#5901) (#5915)
* Fix compare button on upstream repo leading to 404 (#5877) (#5914)

[1.9.2]
* Update Gitea to 1.7.2
* Remove all CommitStatus when a repo is deleted (#5940) (#5941)
* Fix notifications on pushing with deploy keys by setting hook environment variables (#5935) (#5944)
* Silence console logger in gitea serv (#5887) (#5943)
* Handle milestone webhook events for issues and PR (#5947) (#5955)
* Show user who created the repository instead of the organization in action feed (#5948) (#5956)
* Fix ssh deploy and user key constraints (#1357) (#5939) (#5966)
* Fix bug when deleting a linked account will removed all (#5989) (#5990)
* Fix empty ssh key importing in ldap (#5984) (#6009)
* Fix metrics auth token detection (#6006) (#6017)
* Create repository on organisation by default on its dashboard (#6026) (#6048)
* Make sure labels are actually returned in API (#6053) (#6059)
* Switch to more recent build of xgo (#6070) (#6072)
* In basic auth check for tokens before call UserSignIn (#5725) (#6083)

[1.9.3]
* Update Gitea to 1.7.3
* Fix server 500 when trying to migrate to an already existing repository (#6188) (#6197)
* Load Issue attributes for API /repos/{owner}/{repo}/issues/{index} (#6122) (#6185)
* Fix bug whereby user could change private repository to public when force private enabled. (#6156) (#6165)
* Fix bug when update owner team then visit team's repo return 404 (#6119) (#6166)
* Fix heatmap and repository menu display in Internet Explorer 9+ (#6117) (#6137)
* Fix prohibit login check on authorization (#6106) (#6115)
* Fix LDAP protocol error regression by moving to ldap.v3 (#6105) (#6107)
* Fix deadlock in webhook PullRequest (#6102) (#6104)
* Fix redirect loop when password change is required and Gitea is installed as a suburl (#5965) (#6101)
* Fix compare button regression (#5929) (#6098)
* Recover panic in orgmode.Render if bad orgfile (#4982) (#5903) (#6097)

[1.9.4]
* Update Gitea to 1.7.4
* Fix potential XSS vulnerability in repository description. (#6306) (#6308)
* Fix wrong release commit id (#6224) (#6300)
* Fix panic on empty signed commits (#6292) (#6300)
* Fix organization dropdown not being scrollable when using mouse wheel (#5988) (#6246)
* Fix displaying dashboard even if required to change password (#6214) (#6215)

[1.9.5]
* Update Gitea to 1.7.5
* unitTypeCode not being used in accessLevelUnit (#6419) (#6423)
* ParsePatch function to work with quoted diff --git strings (#6323) (#6332)

[1.9.6]
* Update Gitea to 1.7.6
* Prevent remote code execution vulnerability with mirror repo URL settings (#6593) (#6595)
* Allow resend of confirmation email when logged in (#6482) (#6487)

[1.10.0]
* Update Gitea to 1.8.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.8.0)
* Prevent remote code execution vulnerability with mirror repo URL settings (#6593) (#6594)
* Resolve 2FA bypass on API (#6676) (#6674)
* Prevent the creation of empty sessions for non-logged in users (#6690) (#6677)
* Expose issue stopwatch toggling via API (#5970)
* Pull request conflict files detection (#5951)
* Implement "conversation lock" for issue comments (#5073)
* Feature: Archive repos (#5009)
* Allow to set organization visibility (public, internal, private) (#1763)
* Added URL mapping for Release attachments like on github.com (#1707)

[1.10.1]
* Update Gitea to 1.8.1

[1.10.2]
* Update Gitea to 1.8.2

[1.11.0]
* better custom app.ini integration
* optional sso support

[1.12.0]
* Update Gitea to 1.8.3
* Update manifest to v2

[1.13.0]
* Update Gitea to 1.9.0

[1.13.1]
* Update Gitea to 1.9.1

[1.13.2]
* Make sessions persist restarts

[1.13.3]
* Update Gitea to 1.9.2
* Fix wrong sender when send slack webhook (#7918) (#7924)
* Upload support text/plain; charset=utf8 (#7899)
* Lfs/lock: round locked_at timestamp to second (#7872) (#7875)
* Fix non existent milestone with 500 error (#7867) (#7873)
* SECURITY
  * Fix No PGP signature on 1.9.1 tag (#7874)
  * Release built with go 1.12.9 to fix security fixes in golang std lib, ref: https://groups.google.com/forum/#!msg/golang-announce/oeMaeUnkvVE/a49yvTLqAAAJ
* ENHANCEMENT
  * Fix pull creation with empty changes (#7920) (#7926)
* BUILD
  * Drone/docker: prepare multi-arch release + provide arm64 image (#7571) (#7884)

[1.13.4]
* Update Gitea to 1.9.3
* Fix go get from a private repository with Go 1.13 (#8100)
* Strict name matching for Repository.GetTagID() (#8082)
* Avoid ambiguity of branch/directory names for the git-diff-tree command (#8070)
* Add change title notification for issues (#8064)
* Run CORS handler first for /api routes (#7967) (#8053)
* Evaluate emojis in commit messages in list view (#8044)
* Fix failed to synchronize tags to releases for repository (#7990) (#7994)
* Fix adding default Telegram webhook (#7972) (#7992)
* Abort synchronization from LDAP source if there is some error (#7965)
* Fix deformed emoji in commit message (#8071)
* Keep blame view buttons sequence consistent with normal view when viewing a file (#8007) (#8009)

[1.13.5]
* Update Gitea to 1.9.4
* Highlight issue references (#8101) (#8404)
* Fix bug when migrating a private repository #7917 (#8403)
* Change general form binding to gogs form (#8334) (#8402)
* Fix editor commit to new branch if PR disabled (#8375) (#8401)
* Fix milestone num_issues (#8221) (#8400)
* Allow users with explicit read access to give approvals (#8398)
* Fix commit status in PR #8316 and PR #8321 (#8339)
* Fix API for edit and delete release attachment (#8290)
* Fix assets on release webhook (#8283)
* Fix release API URL generation (#8239)
* Allow registration when button is hidden (#8238)
* MS Teams webhook misses commit messages (backport v1.9) (#8225)

[1.13.6]
* Update Gitea to 1.9.5
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.9.5)
* Hide some user information via API if user doesn't have enough permission (#8655) (#8658)
* Fix milestone close timestamp (#8728) (#8731)
* Fix deadline on update issue or PR via API (#8699)
* Fix 'New Issue Missing Milestone Comment' (#8678) (#8682)
* Fix 500 when getting user as unauthenticated user (#8653) (#8662)
* Use AppSubUrl for more redirections (#8647) (#8652)
* Add SubURL to redirect path (#8632) (#8634) (#8640)
* Fix #8582 by handling empty repos (#8587) (#8593)
* Fix bug on pull requests when transfer head repository (#8571)
* Add missed close in ServeBlobLFS (#8527) (#8543)
* Return false if provided branch name is empty for IsBranchExist (#8485) (#8492)
* Create .ssh dir as necessary (#8369) (#8486) (#8489)
* Restore functionality for early gits (#7775) (#8476)
* Add check for empty set when dropping indexes during migration (#8475)
* Ensure Request Body Readers are closed in LFS server (#8454) (#8459)
* Ensure that LFS files are relative to the LFS content path (#8455) (#8458)
* Ignore mentions for users with no access (#8395) (#8484)

[1.14.0]
* Update Gitea to 1.10.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.10.0)

[1.14.1]
* Update Gitea to 1.10.1
* Fix max length check and limit in multiple repo forms (#9148) (#9204)
* Properly fix displaying virtual session provider in admin panel (#9137) (#9203)
* Upgrade levelqueue to 0.1.0 (#9192) (#9199)
* Fix panic when diff (#9187) (#9193)
* Smtp logger configuration sendTos should be an array (#9154) (#9157)
* Always Show Password Field on Link Account Sign-in Page (#9150)
* Create PR on Current Repository by Default (#8670) (#9141)
* Fix race on indexer (#9136) (#9139)
* Fix reCAPTCHA URL (#9119)
* Hide migrated credentials (#9098)
* Update golang.org/x/crypto vendor to use acme v2 (#9056) (#9085)
* Fix password checks on admin create/edit user (#9076) (#9081)
* Fix add search as a reserved username (#9063) (#9065)
* Fix permission checks for close/reopen from commit (#8875) (#9033)
* Ensure Written is set in GZIP ProxyResponseWriter (#9018) (#9025)
* Fix broken link to branch from issue list (#9003) (#9021)
* Fix wrong system notice when repository is empty (#9020)
* Shadow password correctly for session config (#8984) (#9002)

[1.14.2]
* Update Gitea to 1.10.2
* Allow only specific Columns to be updated on Issue via API (#9539) (#9580)
* Add ErrReactionAlreadyExist error (#9550) (#9564)
* Fix bug when migrate from API (#8631) (#9563)
* Use default avatar for ghost user (#9536) (#9537)
* Fix repository issues pagination bug when there are more than one label filter (#9512) (#9528)
* Fix deleted branch not removed when push the branch again (#9516) (#9524)
* Fix missing repository status when migrating repository via API (#9511)
* Trigger webhook when deleting a branch after merging a PR (#9510)
* Fix paging on /repos/{owner}/{repo}/git/trees/{sha} API endpoint (#9482)
* Fix NewCommitStatus (#9434) (#9435)
* Use OriginalURL instead of CloneAddr in migration logging (#9418) (#9420)
* Fix Slack webhook payload title generation to work with Mattermost (#9404)
* DefaultBranch needs to be prefixed by BranchPrefix (#9356) (#9359)
* Fix issue indexer not triggered when migrating a repository (#9333)
* Fix bug that release attachment files not deleted when deleting repository (#9322) (#9329)
* Fix migration releases (#9319) (#9326) (#9328)
* Fix File Edit: Author/Committer interchanged (#9297) (#9300)

[1.14.3]
* Update Gitea to 1.10.3
* Hide credentials when submitting migration (#9102) (#9704)
* Never allow an empty password to validate (#9682) (#9684)
* Prevent redirect to Host (#9678) (#9680)
* Hide public repos owned by private orgs (#9609) (#9616)
* Allow assignee on Pull Creation when Issue Unit is deactivated (#9836) (#9838)
* Fix download file wrong content-type (#9825) (#9835)
* Fix wrong identify poster on a migrated pull request when submit review (#9827) (#9831)
* Fix dump non-exist log directory (#9818) (#9820)
* Fix compare (#9808) (#9815)
* Fix missing msteam webhook on organization (#9781) (#9795)
* Fix add team on collaborator page when same name as organization (#9783)
* Fix cache problem on dashboard (#9358) (#9703)
* Send tag create and push webhook when release created on UI (#8671) (#9702)
* Branches not at ref commit ID should not be listed as Merged (#9614) (#9639)

[1.15.0]
* Update Gitea to 1.11.0

[1.15.1]
* Update Gitea to 1.11.1
* Repo name added to automatically generated commit message when merging (#9997) (#10285)
* Fix Workerpool deadlock (#10283) (#10284)
* Divide GetIssueStats query in smaller chunks (#10176) (#10282)
* Fix reply on code review (#10257)
* Stop hanging issue indexer initialisation from preventing shutdown (#10243) (#10249)
* Fix filter label emoji width (#10241) (#10244)
* Fix issue sidebar menus having an infinite height (#10239) (#10240)
* Fix commit between two commits calculation if there is only last commit (#10225) (#10226)
* Only check for conflicts/merging if the PR has not been merged in the interim (#10132) (#10206)
* Blacklist manifest.json & milestones user (#10292) (#10293)

[1.15.2]
* Update Gitea to 1.11.2

[1.15.3]
* Update Gitea to 1.11.3

[1.15.4]
* Update Gitea to 1.11.4

[1.16.0]
* Update Gitea to [1.11.5](https://github.com/go-gitea/gitea/releases/tag/v1.11.5)
* Update base image to 2.0.0

[1.16.1]
* Update Gitea to 1.11.6
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.11.6)
* Fix missing authorization check on pull for public repos of private/limited org (#11656) (#11683)
* Use session for retrieving org teams (#11438) (#11439)
* Return json on 500 error from API (#11574) (#11660)
* Fix wrong milestone in webhook message (#11596) (#11612)
* Prevent (caught) panic on login (#11590) (#11598)
* Fix commit page js error (#11527)

[1.17.0]
* Update Gitea to 1.12.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.12.0)

[1.18.0]
* Add forumUrl and update tags and screenshots

[1.18.1]
* Update Gitea to 1.12.2
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.12.2)

[1.18.2]
* Update Gitea to 1.12.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.12.3)
* Don't change creation date when updating Release (#12343) (#12351)
* Show 404 page when release not found (#12328) (#12332)
* Fix emoji detection in certain cases (#12320) (#12327)
* Reduce emoji size (#12317) (#12327)
* Fix double-indirection bug in logging IDs (#12294) (#12308)
* Link to pull list page on sidebar when view pr (#12256) (#12263)
* Extend Notifications API and return pinned notifications by default (#12164) (#12232)

[1.18.3]
* Update Gitea to 1.12.4
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.12.4)
* Escape provider name in oauth2 provider redirect (#12648) (#12650)
* Escape Email on password reset page (#12610) (#12612)
* When reading expired sessions - expire them (#12686) (#12690)
* StaticRootPath configurable at compile time (#12371) (#12652)
* Fix to show an issue that is related to a deleted issue (#12651) (#12692)
* Expire time acknowledged for cache (#12605) (#12611)
* Fix diff path unquoting (#12554) (#12575)
* Improve HTML escaping helper (#12562)
* models: break out of loop (#12386) (#12561)
* Default empty merger list to those with write permissions (#12535) (#12560)
* Skip SSPI authentication attempts for /api/internal (#12556) (#12559)
* Prevent NPE on commenting on lines with invalidated comments (#12549) (#12550)
* Remove hardcoded ES indexername (#12521) (#12526)
* Fix bug preventing transfer to private organization (#12497) (#12501)
* Keys should not verify revoked email addresses (#12486) (#12495)
* Do not add prefix on http/https submodule links (#12477) (#12479)
* Fix ignored login on compare (#12476) (#12478)
* Fix incorrect error logging in Stats indexer and OAuth2 (#12387) (#12422)
* Upgrade google/go-github to v32.1.0 (#12361) (#12390)
* Render emoji's of Commit message on feed-page (#12373)
* Fix handling of diff on unrelated branches when Git 2.28 used (#12370)

[1.18.4]
* Update Gitea to 1.12.5
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.12.5)
* Allow U2F with default settings for gitea in subpath (#12990) (#13001)
* Prevent empty div when editing comment (#12404) (#12991)
* On mirror update also update address in DB (#12964) (#12967)
* Allow extended config on cron settings (#12939) (#12943)
* Open transaction when adding Avatar email-hash pairs to the DB (#12577) (#12940)
* Fix internal server error from ListUserOrgs API (#12910) (#12915)
* Update only the repository columns that need updating (#12900) (#12912)
* Fix panic when adding long comment (#12892) (#12894)
* Add size limit for content of comment on action ui (#12881) (#12890)
* Convert User expose ID each time (#12855) (#12883)
* Support slashes in release tags (#12864) (#12882)
* Add missing information to CreateRepo API endpoint (#12848) (#12867)
* On Migration respect old DefaultBranch (#12843) (#12858)
* Fix notifications page links (#12838) (#12853)
* Stop cloning unnecessarily on PR update (#12839) (#12852)
* Escape more things that are passed through str2html (#12622) (#12850)
* Remove double escape on labels addition in comments (#12809) (#12810)
* Fix "only mail on mention" bug (#12775) (#12789)
* Fix yet another bug with diff file names (#12771) (#12776)
* RepoInit Respect AlternateDefaultBranch (#12746) (#12751)
* Fix Avatar Resize (resize algo NearestNeighbor -> Bilinear) (#12745) (#12750)

[1.18.5]
* Update Gitea to 1.12.6
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.12.6)
* Prevent git operations for inactive users (#13527) (#13537)
* Disallow urlencoded new lines in git protocol paths if there is a port (#13521) (#13525)
* API should only return Json (#13511) (#13564)
* Fix before and since query arguments at API (#13559) (#13560)
* Prevent panic on git blame by limiting lines to 4096 bytes at most (#13470) (#13492)
* Fix link detection in repository description with tailing '_' (#13407) (#13408)
* Remove obsolete change of email on profile page (#13341) (#13348)
* Fix permission check on get Reactions API endpoints (#13344) (#13346)
* Add migrated pulls to pull request task queue (#13331) (#13335)
* API deny wrong pull creation options (#13308) (#13327)
* Fix initial commit page & binary munching problem (#13249) (#13259)
* Fix diff parsing (#13157) (#13136) (#13139)
* Return error 404 not 500 from API if team does not exist (#13118) (#13119)
* Prohibit automatic downgrades (#13108) (#13111)
* Fix GitLab Migration Option AuthToken (#13101)
* GitLab Label Color Normalizer (#12793) (#13100)
* Log the underlying panic in runMigrateTask (#13096) (#13098)
* Fix attachments list in edit comment (#13036) (#13097)
* Fix deadlock when deleting team user (#13093)
* Fix error create comment on outdated file (#13041) (#13042)
* Fix repository create/delete event webhooks (#13008) (#13027)
* Fix internal server error on README in submodule (#13006) (#13016)

[1.19.0]
* Update Gitea to 1.13.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.13.0)

[1.19.1]
* Update Gitea to 1.13.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.13.1)
* Security: Hide private participation in Orgs (#13994) (#14031)
* Security: Fix escaping issue in diff (#14153) (#14154)

[1.19.2]
* Update Gitea to 1.13.2
* Prevent panic on fuzzer provided string (#14405) (#14409)
* Add secure/httpOnly attributes to the lang cookie (#14279) (#14280)
* If release publisher is deleted use ghost user (#14375)
* Internal ssh server respect Ciphers, MACs and KeyExchanges settings (#14523) (#14530)
* Set the name Mapper in migrations (#14526) (#14529)
* Fix wiki preview (#14515)
* Update code.gitea.io/sdk/gitea v0.13.1 -> v0.13.2 (#14497)
* ChangeUserName: rename user files back on DB issue (#14447)
* Fix lfs preview bug (#14428) (#14433)
* Ensure timeout error is shown on u2f timeout (#14417) (#14431)
* Fix Deadlock & Delete affected reactions on comment deletion (#14392) (#14425)
* Use path not filepath in routers/editor (#14390) (#14396)
* Check if label template exist first (#14384) (#14389)
* Fix migration v141 (#14387) (#14388)
* Use Request.URL.RequestURI() for fcgi (#14347)
* Use ServerError provided by Context (#14333) (#14345)
* Fix edit-label form init (#14337)
* Fix mailIssueCommentBatch for pull request (#14252) (#14296)
* Render links for commit hashes followed by comma (#14224) (#14227)
* Send notifications for mentions in pulls, issues, (code-)comments (#14218) (#14221)
* Fix avatar bugs (#14217) (#14220)
* Ensure that schema search path is set with every connection on postgres (#14131) (#14216)
* Fix dashboard issues labels filter bug (#14210) (#14214)
* When visit /favicon.ico but the static file is not exist return 404 but not continue to handle the route (#14211) (#14213)
* Fix branch selector on new issue page (#14194) (#14207)
* Check for notExist on profile repository page (#14197) (#14203)

[1.20.0]
* Use base image v3

[1.20.1]
* Update Gitea to 1.13.3
* Turn default hash password algorithm back to pbkdf2 from argon2 until we find a better one (#14673) (#14675)
* Fix paging of file commit logs (#14831) (#14879)
* Print useful error if SQLite is used in settings but not supported (#14476) (#14874)
* Fix display since time round (#14226) (#14873)
* When Deleting Repository only explicitly close PRs whose base is not this repository (#14823) (#14842)
* Set HCaptchaSiteKey on Link Account pages (#14834) (#14839)
* Fix a couple of CommentAsPatch issues. (#14804) (#14820)
* Disable broken OAuth2 providers at startup (#14802) (#14811)
* Repo Transfer permission checks (#14792) (#14794)
* Fix double alert in oauth2 application edit view (#14764) (#14768)
* Fix broken spans in diffs (#14678) (#14683)
* Prevent race in PersistableChannelUniqueQueue.Has (#14651) (#14676)
* HasPreviousCommit causes recursive load of commits unnecessarily (#14598) (#14649)
* Do not assume all 40 char strings are SHA1s (#14624) (#14648)
* Allow org labels to be set with issue templates (#14593) (#14647)
* Accept multiple SSH keys in single LDAP SSHPublicKey attribute (#13989) (#14607)
* Fix bug about ListOptions and stars/watchers pagnation (#14556) (#14573)
* Fix GPG key deletion during account deletion (#14561) (#14569)

[1.20.2]
* Update Gitea to 1.13.4
* Fix issue popups (#14898) (#14899)
* Fix race in LFS ContentStore.Put(...) (#14895) (#14913)
* Fix a couple of issues with a feeds (#14897) (#14903)
* When transfering repository and database transaction failed, rollback the renames (#14864) (#14902)
* Fix race in local storage (#14888) (#14901)
* Fix 500 on pull view page if user is not loged in (#14885) (#14886)

[1.20.3]
* Update Gitea to 1.13.5
* Update to goldmark 1.3.3 (#15059) (#15061)
* Another clusterfuzz spotted issue (#15032) (#15034)
* Fix set milestone on PR creation (#14981) (#15001)
* Prevent panic when editing forked repos by API (#14960) (#14963)
* Fix bug when upload on web (#15042) (#15055)
* Delete Labels & IssueLabels on Repo Delete too (#15039) (#15051)
* Fix postgres ID sequences broken by recreate-table (#15015) (#15029)
* Fix several render issues (#14986) (#15013)
* Make sure sibling images get a link too (#14979) (#14995)
* Fix Anchor jumping with escaped query components (#14969) (#14977)
* Fix release mail html template (#14976)
* Fix excluding more than two labels on issues list (#14962) (#14973)
* Don't mark each comment poster as OP (#14971) (#14972)
* Add "captcha" to list of reserved usernames (#14930)
* Re-enable import local paths after reversion from #13610 (#14925) (#14927)

[1.20.4]
* Update Gitea to 1.13.6
* Fix bug on avatar middleware (#15124) (#15125)
* Fix another clusterfuzz identified issue (#15096) (#15114)
* Fix nil exeption for get pull reviews API #15104 (#15106)
* Fix markdown rendering in milestone content (#15056) (#15092)

[1.20.5]
* Update Gitea to 1.13.7
* Update to bluemonday-1.0.6 (#15294) (#15298)
* Clusterfuzz found another way (#15160) (#15169)
* Fix wrong user returned in API (#15139) (#15150)
* Add 'fonts' into 'KnownPublicEntries' (#15188) (#15317)
* Speed up enry.IsVendor (#15213) (#15246)
* Response 404 for diff/patch of a commit that not exist (#15221) (#15238)
* Prevent NPE in CommentMustAsDiff if no hunk header (#15199) (#15201)
* Add size to Save function (#15264) (#15271)

[1.21.0]
* Update Gitea to 1.14.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.14.0)

[1.21.1]
* Update Gitea to 1.14.1
* Fix bug clone wiki (#15499) (#15502)
* Github Migration ignore rate limit, if not enabled (#15490) (#15495)
* Use subdir for URL (#15446) (#15493)
* Query the DB for the hash before inserting in to email_hash (#15457) (#15491)
* Ensure review dismissal only dismisses the correct review (#15477) (#15489)
* Use index of the supported tags to choose user lang (#15452) (#15488)
* Fix wrong file link in code search page (#15466) (#15486)
* Quick template fix for built-in SSH server in admin config (#15464) (#15481)
* Prevent superfluous response.WriteHeader (#15456) (#15476)
* Fix ambiguous argument error on tags (#15432) (#15474)
* Add created_unix instead of expiry to migration (#15458) (#15463)
* Fix repository search (#15428) (#15442)
* Prevent NPE on avatar direct rendering if federated avatars disabled (#15434) (#15439)
* Fix wiki clone urls (#15430) (#15431)
* Fix dingtalk icon url at webhook (#15417) (#15426)
* Standardise icon on projects PR page (#15387) (#15408)
* Add option to skip LFS/attachment files for dump (#15407) (#15492)
* Clone panel fixes (#15436)
* Use semantic dropdown for code search query type (#15276) (#15364)

[1.21.2]
* Update Gitea to 1.14.2
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.14.2)
* Display conflict-free merge messages for pull requests (#15773) (#15796)
* Exponential Backoff for ByteFIFO (#15724) (#15793)
* Issue list alignment tweaks (#15483) (#15766)
* Implement delete release attachments and update release attachments' name (#14130) (#15666)
* Add placeholder text to deploy key textarea (#15575) (#15576)
* Project board improvements (#15429) (#15560)
* Repo branch page: label size, PR ref, new PR button alignment (#15363) (#15365)

[1.21.3]
* Update Gitea to 1.14.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.14.3)
* Encrypt migration credentials at rest (#15895) (#16187)
* Only check access tokens if they are likely to be tokens (#16164) (#16171)
* Add missing SameSite settings for the i_like_gitea cookie (#16037) (#16039)
* Fix setting of SameSite on cookies (#15989) (#15991)

[1.21.4]
* Update Gitea to 1.14.4
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.14.4)
* Fix relative links in postprocessed images (#16334) (#16340)
* Fix list_options GetStartEnd (#16303) (#16305)
* Fix API to use author for commits instead of committer (#16276) (#16277)
* Handle misencoding of login_source cfg in mssql (#16268) (#16275)
* Fixed issues not updated by commits (#16254) (#16261)
* Improve efficiency in FindRenderizableReferenceNumeric and getReference (#16251) (#16255)
* Use html.Parse rather than html.ParseFragment (#16223) (#16225)
* Fix milestone counters on new issue (#16183) (#16224)
* reqOrgMembership calls need to be preceded by reqToken (#16198) (#16219)

[1.21.5]
* Update Gitea to 1.14.5
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.14.5)
* Hide mirror passwords on repo settings page (#16022) (#16355)
* Update bluemonday to v1.0.15 (#16379) (#16380)
* Retry rename on lock induced failures (#16435) (#16439)
* Validate issue index before querying DB (#16406) (#16410)
* Fix crash following ldap authentication update (#16447) (#16449)
* Redirect on bad CSRF instead of presenting bad page (#14937) (#16378)

[1.21.6]
* Update Gitea to 1.14.6
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.14.6)
* SECURITY
  * Bump github.com/markbates/goth from v1.67.1 to v1.68.0 (#16538) (#16540)
  * Switch to maintained JWT lib (#16532) (#16535)
  * Upgrade to latest version of golang-jwt (as forked for 1.14) (#16590) (#16607)

[1.22.0]
* Update Gitea to 1.15.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.0)

[1.22.1]
* Update Gitea to 1.15.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.1)

[1.22.2]
* Update Gitea to 1.15.2
* Add unique constraint back into issue_index (#16938)
* Close storage objects before cleaning (#16934) (#16942)

[1.22.3]
* Update Gitea to 1.15.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.3)
* Add fluid to ui container class to remove margin (#16396) (#16976)
* Add caller to cat-file batch calls (#17082) (#17089)
* Many bug fixes

[1.22.4]
* Update Gitea to 1.15.4
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.4)
* Raw file API: don't try to interpret 40char filenames as commit SHA (#17185) (#17272)
* Don't allow merged PRs to be reopened (#17192) (#17271)
* Fix incorrect repository count on organization tab of dashboard (#17256) (#17266)
* Fix unwanted team review request deletion (#17257) (#17264)
* Fix broken Activities link in team dashboard (#17255) (#17258)
* API pull's head/base have correct permission(#17214) (#17245)
* Fix stange behavior of DownloadPullDiffOrPatch in incorect index (#17223) (#17227)
* Upgrade xorm to v1.2.5 (#17177) (#17188)
* Fix missing repo link in issue/pull assigned emails (#17183) (#17184)
* Fix bug of get context user (#17169) (#17172)
* Nicely handle missing user in collaborations (#17049) (#17166)
* Add Horizontal scrollbar to inner menu on Chrome (#17086) (#17164)
* Fix wrong i18n keys (#17150) (#17153)
* Fix Archive Creation: correct transaction ending (#17151)
* Prevent panic in Org mode HighlightCodeBlock (#17140) (#17141)
* Create doctor command to fix repo_units broken by dumps from 1.14.3-1.14.6 (#17136) (#17137)

[1.22.5]
* Update Gitea to 1.15.5
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.5)
* Upgrade Bluemonday to v1.0.16 (#17372) (#17374)
* Ensure correct SSH permissions check for private and restricted users (#17370) (#17373)
* Prevent NPE in CSV diff rendering when column removed (#17018) (#17377)
* Offer rsa-sha2-512 and rsa-sha2-256 algorithms in internal SSH (#17281) (#17376)
* Don't panic if we fail to parse U2FRegistration data (#17304) (#17371)
* Ensure popup text is aligned left (backport for 1.15) (#17343)
* Ensure that git daemon export ok is created for mirrors (#17243) (#17306)
* Disable core.protectNTFS (#17300) (#17302)
* Use pointer for wrappedConn methods (#17295) (#17296)
* AutoRegistration is supposed to be working with disabled registration (backport) (#17292)
* Handle duplicate keys on GPG key ring (#17242) (#17284)
* Fix SVG side by side comparison link (#17375) (#17391)

[1.22.6]
* Update Gitea to 1.15.6
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.6)
* Prevent panic in serv.go with Deploy Keys (#17434) (#17435)
* Fix CSV render error (#17406) (#17431)
* Read expected buffer size (#17409) (#17430)
* Ensure that restricted users can access repos for which they are members (#17460) (#17464)
* Make commit-statuses popup show correctly (#17447) (#17466)

[1.23.0]
* Update Gitea to 1.15.7
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.7)
* Enable rendering of jupyter notebooks, rst, asciidoc by default
* Only allow webhook to send requests to allowed hosts (#17482) (#17510)
* Fix login redirection links (#17451) (#17473)

[1.23.1]
* Update Gitea to 1.15.8
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.8)
* Move POST /{username}/action/{action} to simply POST /{username} (#18045) (#18046)
* Fix delete u2f keys bug (#18040) (#18042)
* Reset Session ID on login (#18018) (#18041)
* Prevent off-by-one error on comments on newly appended lines (#18029) (#18035)
* Stop printing 03d after escaped characters in logs (#18030) (#18034)

[1.23.2]
* Update Gitea to 1.15.9
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.9)
* Fix wrong redirect on org labels (#18128) (#18134)
* Fix: unstable sort skips/duplicates issues across pages (#18094) (#18095)
* Revert "Fix delete u2f keys bug (#18042)" (#18107)
* Migrating wiki don't require token, so we should move it out of the require form (#17645) (#18104)
* Prevent NPE if gitea uploader fails to open url (#18080) (#18101)
* Reset locale on login (#17734) (#18100)
* Correctly handle failed migrations (#17575) (#18099)
* Instead of using routerCtx just escape the url before routing (#18086) (#18098)
* Quote references to the user table in consistency checks (#18072) (#18073)
* Add NotFound handler (#18062) (#18067)
* Ensure that git repository is closed before transfer (#18049) (#18057)
* Use common sessioner for API and web routes (#18114)

[1.23.3]
* Update Gitea to 1.15.10
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.15.10)
* Fix inconsistent PR comment counts (#18260) (#18261)
* Fix release link broken (#18252) (#18253)
* Fix update user from site administration page bug (#18250) (#18251)
* Set HeadCommit when creating tags (#18116) (#18173)
* Use correct translation key for error messages due to max repo limits (#18135 & #18153) (#18152)
* Fix purple color in suggested label colors (#18241) (#18242)
* Bump mermaid from 8.10.1 to 8.13.8 (#18198) (#18206)

[1.23.4]
* Update Gitea to 1.16.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.16.0)

[1.23.5]
* Update Gitea to 1.16.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.16.1)
* Update JS dependencies, fix lint (#18389) (#18540)
* Add dropdown icon to label set template dropdown (#18564) (#18571)
* Comments on migrated issues/prs must link to the comment ID (#18630) (#18637)
* Stop logging an error when notes are not found (#18626) (#18635)
* Ensure that blob-excerpt links work for wiki (#18587) (#18624)
* Only attempt to flush queue if the underlying worker pool is not finished (#18593) (#18620)

[1.23.6]
* Update Gitea to 1.16.2
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.16.2)
* Show fullname on issue edits and gpg/ssh signing info (#18828)
* Immediately Hammer if second kill is sent (#18823) (#18826)
* Allow mermaid render error to wrap (#18791)

[1.23.7]
* Update Gitea to 1.16.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.16.3)

[1.23.8]
* Update Gitea to 1.16.4
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.16.4)
* Restrict email address validation (#17688) (#19085)
* Fix lfs bug (#19072) (#19080)

[1.23.9]
* Update Gitea to 1.16.5
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.16.5)
* Prevent redirect to Host (2) (#19175) (#19186)
* Try to prevent autolinking of displaynames by email readers (#19169) (#19183)
* Clean paths when looking in Storage (#19124) (#19179)
* Do not send notification emails to inactive users (#19131) (#19139)
* Do not send activation email if manual confirm is set (#19119) (#19122)

[1.23.10]
* Update Gitea to 1.16.6
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.16.6)

[1.23.11]
* Update Gitea to 1.16.7
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.16.7)
* Escape git fetch remote (#19487) (#19490)
* On Migrations, only write commit-graph if wiki clone was successful (#19563) (#19568)
* Respect DefaultUserIsRestricted system default when creating new user (#19310) (#19560)
* Don't error when branch's commit doesn't exist (#19547) (#19548)
* Support hostname:port to pass host matcher's check (#19543) (#19544)
* Prevent dangling archiver goroutine (#19516) (#19526)
* Fix migrate release from github (#19510) (#19523)
* When view `_Siderbar` or `_Footer`, just display once (#19501) (#19522)
* Fix blame page select range error and some typos (#19503)
* Fix name of doctor fix "authorized-keys" in hints (#19464) (#19484)
* User specific repoID or xorm builder conditions for issue search (#19475) (#19476)
* Prevent dangling cat-file calls (goroutine alternative) (#19454) (#19466)
* RepoAssignment ensure to close before overwrite (#19449) (#19460)
* Set correct PR status on 3way on conflict checking (#19457) (#19458)
* Mark TemplateLoading error as "UnprocessableEntity" (#19445) (#19446)

[1.23.12]
* Update Gitea to 1.16.8
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.16.8)
* Add doctor check/fix for bogus action rows (#19656) (#19669)
* Make .cs highlighting legible on dark themes (#19604) (#19605)
* Delete user related oauth stuff on user deletion too (#19677) (#19680)
* Fix new release from tags list UI (#19670) (#19673)
* Prevent NPE when checking repo units if the user is nil (#19625) (#19630)

[1.24.0]
* Update Gitea to 1.17.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.17.0)
* Automatically render wiki TOC (#19873)
* Adding button to link accounts from user settings (#19792)
* Allow set default merge style while creating repo (#19751)
* Auto merge pull requests when all checks succeeded (#9307 & #19648)
* Improve reviewing PR UX (#19612)
* Add support for rendering console output with colors (#19497)

[1.24.1]
* Update Gitea to 1.17.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.17.1)
* Correctly escape within tribute.js (#20831) (#20832)
* Add support for NuGet API keys (#20721) (#20734)
* Display project in issue list (#20583)
* Add disable download source configuration (#20548) (#20579)
* Add username check to doctor (#20140) (#20671)
* Enable Wire 2 for Internal SSH Server (#20616) (#20617)
* Use the total issue count for UI (#20785) (#20827)
* Add proxy host into allow list (#20798) (#20819)
* Add missing translation for queue flush workers (#20791) (#20792)
* Improve comment header for mobile (#20781) (#20789)
* Fix git.Init for doctor sub-command (#20782) (#20783)
* Check webhooks slice length before calling xorm (#20642) (#20768)
* Remove manual rollback for failed generated repositories (#20639) (#20762)
* Use correct field name in npm template (#20675) (#20760)
* Keep download count on Container tag overwrite (#20728) (#20735)
* Fix v220 migration to be compatible for MSSQL 2008 r2 (#20702) (#20707)
* Use request timeout for git service rpc (#20689) (#20693)
* Send correct NuGet status codes (#20647) (#20677)
* Use correct context to get package content (#20673) (#20676)
* Fix the JS error "EventSource is not defined" caused by some non-standard browsers (#20584) (#20663)
* Add default commit messages to PR for squash merge (#20618) (#20645)
* Fix package upload for files >32mb (#20622) (#20635)
* Fix the new-line copy-paste for rendered code (#20612)
* Clean up and fix clone button script (#20415 & #20600) (#20599)
* Fix default merge style (#20564) (#20565)
* Add repository condition for issue count (#20454) (#20496)
* Make branch icon stand out more (#20726) (#20774)
* Fix loading button with invalid form (#20754) (#20759)
* Fix SecToTime edge-cases (#20610) (#20611)
* Executable check always returns true for windows (#20637) (#20835)
* Check issue labels slice length before calling xorm Insert (#20655) (#20836)
* Fix owners cannot create organization repos bug (#20841) (#20854)
* Prevent 500 is head repo does not have PullRequest unit in IsUserAllowedToUpdate (#20839) (#20848)

[1.24.2]
* Update Gitea to 1.17.2
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.17.2)
* Double check CloneURL is acceptable (#20869) (#20892)
* Add more checks in migration code (#21011) (#21050)
* Fix hard-coded timeout and error panic in API archive download endpoint (#20925) (#21051)
* Improve arc-green code theme (#21039) (#21042)
* Enable contenthash in filename for dynamic assets (#20813) (#20932)
* Don't open new page for ext wiki on same repository (#20725) (#20910)
* Disable doctor logging on panic (#20847) (#20898)
* Remove calls to load Mirrors in user.Dashboard (#20855) (#20897)
* Update codemirror to 5.65.8 (#20875)
* Rework repo buttons (#20602, #20718) (#20719)
* Ensure delete user deletes all comments (#21067) (#21068)
* Delete unreferenced packages when deleting a package version (#20977) (#21060)
* Redirect if user does not exist on admin pages (#20981) (#21059)
* Set uploadpack.allowFilter etc on gitea serv to enable partial clones with ssh (#20902) (#21058)
* Fix 500 on time in timeline API (#21052) (#21057)
* Fill the specified ref in webhook test payload (#20961) (#21055)
* Add another index for Action table on postgres (#21033) (#21054)
* Fix broken insecureskipverify handling in redis connection uris (#20967) (#21053)
* Add Dev, Peer and Optional dependencies to npm PackageMetadataVersion (#21017) (#21044)
* Do not add links to Posters or Assignees with ID < 0 (#20577) (#21037)
* Fix modified due date message (#20388) (#21032)
* Fix missed sort bug (#21006)
* Fix input.value attr for RequiredClaimName/Value (#20946) (#21001)
* Change review buttons to icons to make space for text (#20934) (#20978)
* Fix download archiver of a commit (#20962) (#20971)
* Return 404 NotFound if requested attachment does not exist (#20886) (#20941)
* Set no-tags in git fetch on compare (#20893) (#20936)
* Allow multiple metadata files for Maven packages (#20674) (#20916)
* Increase Content field size of gpg_key and public_key to MEDIUMTEXT (#20896) (#20911)
* Fix mirror address setting not working (#20850) (#20904)
* Fix push mirror address backend get error Address cause setting page display error (#20593) (#20901)
* Fix panic when an invalid oauth2 name is passed (#20820) (#20900)
* In PushMirrorsIterate and MirrorsIterate if limit is negative do not set it (#20837) (#20899)
* Ensure that graceful start-up is informed of unused SSH listener (#20877) (#20888)
* Pad GPG Key ID with preceding zeroes (#20878) (#20885)
* Fix SQL Query for SearchTeam (#20844) (#20872)
* Fix the mode of custom dir to 0700 in docker-rootless (#20861) (#20867)
* Fix UI mis-align for PR commit history (#20845) (#20859)

[1.24.3]
* Update Gitea to 1.17.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.17.3)
* SECURITY
  * Sanitize and Escape refs in git backend (#21464) (#21463)
  * Bump golang.org/x/text (#21412) (#21413)
  * Update bluemonday (#21281) (#21287)
* ENHANCEMENTS
  * Fix empty container layer history and UI (#21251) (#21278)
  * Use en-US as fallback when using other default language (#21200) (#21256)
  * Make the vscode clone link respect transport protocol (#20557) (#21128)

[1.24.4]
* Make email display name configurable

[1.24.5]
* Support autosigning via gnupg

[1.25.0]
* Update Gitea to 1.17.4
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.17.4)
* Do not allow Ghost access to limited visible user/org (#21849) (#21875)
* Fix package access for admins and inactive users (#21580) (#21592)
* Fix button in branch list, avoid unexpected page jump before restore branch actually done (#21562) (#21927)
* Fix vertical align of committer avatar rendered by email address (#21884) (#21919)
* Fix setting HTTP headers after write (#21833) (#21874)
* Ignore line anchor links with leading zeroes (#21728) (#21777)
* Enable Monaco automaticLayout (#21516)

[1.25.1]
* Trust reverse proxy IP

[1.26.0]
* Update Gitea to 1.18.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.18.0)
* Remove ReverseProxy authentication from the API (#22219) (#22251)
* Support Go Vulnerability Management (#21139)
* Forbid HTML string tooltips (#20935)
* Rework mailer settings (#18982)
* Remove U2F support (#20141)
* Refactor i18n to locale (#20153)
* Enable contenthash in filename for dynamic assets (#20813)
* And a lot more enhancements

[1.26.1]
* Update Gitea to 1.18.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.18.1)
* Update github.com/zeripath/zapx/v15 (#22485)
* Fix pull request API field closed_at always being null (#22482) (#22483)
* Fix container blob mount (#22226) (#22476)
* Fix error when calculating repository size (#22392) (#22474)
* Fix Operator does not exist bug on explore page with ONLY_SHOW_RELEVANT_REPOS (#22454) (#22472)
* Fix environments for KaTeX and error reporting (#22453) (#22473)
* Remove the netgo tag for Windows build (#22467) (#22468)
* Fix migration from GitBucket (#22477) (#22465)
* Prevent panic on looking at api "git" endpoints for empty repos (#22457) (#22458)
* Fix PR status layout on mobile (#21547) (#22441)
* Fix wechatwork webhook sends empty content in PR review (#21762) (#22440)
* Remove duplicate "Actions" label in mobile view (#21974) (#22439)
* Fix leaving organization bug on user settings -> orgs (#21983) (#22438)
* Fixed colour transparency regex matching in project board sorting (#22092) (#22437)
* Correctly handle select on multiple channels in Queues (#22146) (#22428)
* Prepend refs/heads/ to issue template refs (#20461) (#22427)
* Restore function to "Show more" buttons (#22399) (#22426)
* Continue GCing other repos on error in one repo (#22422) (#22425)
* Allow HOST has no port (#22280) (#22409)
* Fix omit `avatar_url` in discord payload when empty (#22393) (#22394)
* Don't display stop watch top bar icon when disabled and hidden when click other place (#22374) (#22387)
* Don't lookup mail server when using sendmail (#22300) (#22383)
* Fix gravatar disable bug (#22337)
* Fix update settings table on install (#22326) (#22327)
* Fix sitemap (#22272) (#22320)
* Fix code search title translation (#22285) (#22316)
* Fix due date rendering the wrong date in issue (#22302) (#22306)
* Fix get system setting bug when enabled redis cache (#22298)
* Fix bug of DisableGravatar default value (#22297)
* Fix key signature error page (#22229) (#22230)

[1.26.2]
* Update Gitea to 1.18.2
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.18.2)
* When updating by rebase we need to set the environment for head repo (#22535) (#22536)
* Fix issue not auto-closing when it includes a reference to a branch (#22514) (#22521)
* Fix invalid issue branch reference if not specified in template (#22513) (#22520)
* Fix 500 error viewing pull request when fork has pull requests disabled (#22512) (#22515)
* Reliable selection of admin user (#22509) (#22511)
* Set `disable_gravatar`/`enable_federated_avatar` when offline mode is true (#22479) (#22496)

[1.26.3]
* Update Gitea to 1.18.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.18.3)
* Prevent multiple To recipients (#22566) (#22569)
* Truncate commit summary on repo files table. (#22551) (#22552)
* Mute all links in issue timeline (#22534)

[1.26.4]
* Update Gitea to 1.18.4
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.18.4)
* SECURITY
  * Provide the ability to set password hash algorithm parameters (#22942) (#22943)
  * Add command to bulk set must-change-password (#22823) (#22928)
* ENHANCEMENTS
  * Use import of OCI structs (#22765) (#22805)
  * Fix color of tertiary button on dark theme (#22739) (#22744)
  * Link issue and pull requests status change in UI notifications directly to their event in the timelined view. (#22627) (#22642)
* BUGFIXES
  * Notify on container image create (#22806) (#22965)
  * Fix blame view missing lines (#22826) (#22929)
  * Fix incorrect role labels for migrated issues and comments (#22914) (#22923)
  * Fix PR file tree folders no longer collapsing (#22864) (#22872)
  * Escape filename when assemble URL (#22850) (#22871)

[1.27.0]
* Email display name support

[1.27.1]
* Update Gitea to 1.18.5
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.18.5)
* Hide 2FA status from other members in organization members list (#22999) (#23023)
* Add `force_merge` to merge request and fix checking mergable (#23010) (#23032)
* Use --message=%s for git commit message (#23028) (#23029)
* Render access log template as text instead of HTML (#23013) (#23025)
* Fix the Manually Merged form (#23015) (#23017)

[1.28.0]
* Update Gitea to 1.19.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.19.0)

[1.28.1]
* Update Gitea to 1.19.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.19.1)
* Breaking: Rename actions unit to repo.actions and add docs for it (#23733) (#23881)
* Add card type to org/user level project on creation, edit and view (#24043) (#24066)
* Refactor commit status for Actions jobs (#23786) (#24060)
* Show errors for KaTeX and mermaid on the preview tab (#24009) (#24019)
* Show protected branch rule names again (#23907) (#24018)
* Adjust sticky PR header to cover background (#23956) (#23999)
* Discolor pull request tab labels (#23950) (#23987)
* Treat PRs with agit flow as fork PRs when triggering actions. (#23884) (#23967)
* Left-align review comments (#23937)

[1.28.2]
* Update Gitea to 1.19.2
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.19.2)
* Require repo scope for PATs for private repos and basic authentication (#24362) (#24364)
* Only delete secrets belonging to its owner (#24284) (#24286)
* Fix typo in API route (#24310) (#24332)
* Fix access token issue on some public endpoints (#24194) (#24259)
* Fix broken clone script on an empty archived repo (#24339) (#24348)
* Fix Monaco IOS keyboard button (#24341) (#24347)
* Don't set meta theme-color by default (#24340) (#24346)

[1.28.3]
* Update Gitea to 1.19.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.19.3)
* Use golang 1.20.4 to fix CVE-2023-24539, CVE-2023-24540, and CVE-2023-29400
* Enable whitespace rendering on selection in Monaco (#24444) (#24485)
* Improve milestone filter on issues page (#22423) (#24440)
* Fix api error message if fork exists (#24487) (#24493)
* Fix user-cards format (#24428) (#24431)

[1.28.4]
* Update Gitea to 1.19.4
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.19.4)
* Fix open redirect check for more cases (#25143) (#25155)
* Return 404 in the API if the requested webhooks were not found (#24823) (#24830)
* Fix organization field being null in GET /api/v1/teams/{id} (#24694) (#24696)
* Set --font-weight-bold to 600 (#24840)
* Make mailer SMTP check have timed context (#24751) (#24759)
* Do not select line numbers when selecting text from the action run logs (#24594) (#24596)

[1.29.0]
* Update Gitea to 1.20.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.20.0)
* [Blog](https://blog.gitea.com/release-of-1.20.0/)
* Add button on diff header to copy file name, misc diff header tweaks (#24986)
* API endpoint for changing/creating/deleting multiple files (#24887)
* Support changing git config through app.ini, use diff.algorithm=histogram by default (#24860)
* Add up and down arrows to selected lookup repositories (#24727)
* Add Go package registry (#24687)
* Add status indicator on main home screen for each repo (#24638)
* Support for status check pattern (#24633)

[1.29.1]
* Update Gitea to 1.20.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.20.1)
* Disallow dangerous URL schemes (#25960) (#25964)
* Show the mismatched ROOT_URL warning on the sign-in page if OAuth2 is enabled (#25947) (#25972)
* Make pending commit status yellow again (#25935) (#25968)
* Fix version in rpm repodata/primary.xml.gz (#26009) (#26048)
* Fix env config parsing for "GITEA____APP_NAME" (#26001) (#26013)
* ParseScope with owner/repo always sets owner to zero (#25987) (#25989)
* Fix SSPI auth panic (#25955) (#25969)
* Avoid creating directories when loading config (#25944) (#25957)
* Make environment-to-ini work with INSTALL_LOCK=true (#25926) (#25937)
* Ignore `runs-on` with expressions when warning no matched runners (#25917) (#25933)
* Avoid opening/closing PRs which are already merged (#25883) (#25903)
* RPM Registry: Show zypper commands for SUSE based distros as well (#25981) (#26020)
* Correctly refer to dev tags as nightly in the docker docs (#26004) (#26019)
* Update path related documents (#25417) (#25982)
* Adding remaining enum for migration repo model type. (#26021) (#26034)
* Fix the route for pull-request's authors (#26016) (#26018)
* Fix commit status color on dashboard repolist (#25993) (#25998)
* Avoid hard-coding height in language dropdown menu (#25986) (#25997)
* Add shutting down notice (#25920) (#25922)
* Fix incorrect milestone count when provide a keyword (#25880) (#25904)

[1.29.2]
* Update Gitea to 1.20.2
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.20.2)
* Calculate `MAX_WORKERS` default value by CPU number (#26177) (#26183)
* Display deprecated warning in admin panel pages as well as in the log file (#26094) (#26154)

[1.29.3]
* Update Gitea to 1.20.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.20.3)
* Fix the wrong derive path (#26271) (#26318)
* Fix API leaking Usermail if not logged in (#25097) (#26350)
* Add ThreadID parameter for Telegram webhooks (#25996) (#26480)
* Add minimum polyfill to support "relative-time-element" in PaleMoon (#26575) (#26578)
* Fix dark theme highlight for "NameNamespace" (#26519) (#26527)
* Detect ogg mime-type as audio or video (#26494) (#26505)
* Use object-fit: contain for oauth2 custom icons (#26493) (#26498)
* Move dropzone progress bar to bottom to show filename when uploading (#26492) (#26497)
* Remove last newline from config file (#26468) (#26471)
* Minio: add missing region on client initialization (#26412) (#26438)
* Add pull request review request webhook event (#26401) (#26407)
* Fix text truncate (#26354) (#26384)
* Fix incorrect color of selected assignees when create issue (#26324) (#26372)
* Display human-readable text instead of cryptic filemodes (#26352) (#26358)
* Hide last indexed SHA when a repo could not be indexed yet (#26340) (#26345)
* Fix the topic validation rule and suport dots (#26286) (#26303)
* Fix due date rendering the wrong date in issue (#26268) (#26274)
* Don't autosize textarea in diff view (#26233) (#26244)
* Fix commit compare style (#26209) (#26226)
* Warn instead of reporting an error when a webhook cannot be found (#26039) (#26211)
* Use "input" event instead of "keyup" event for migration form (#26602) (#26605)
* Do not use deprecated log config options by default (#26592) (#26600)
* Fix "issueReposQueryPattern does not match query" (#26556) (#26564)
* Sync repo's IsEmpty status correctly (#26517) (#26560)
* Fix project filter bugs (#26490) (#26558)
* Use hidden over clip for text truncation (#26520) (#26522)
* Set "type=button" for editor's toolbar buttons (#26510) (#26518)
* Fix NuGet search endpoints (#25613) (#26499)
* Fix storage path logic especially for relative paths (#26441) (#26481)
* Close stdout correctly for "git blame" (#26470) (#26473)
* Check first if minio bucket exists before trying to create it (#26420) (#26465)
* Avoiding accessing undefined tributeValues #26461 (#26462)
* Call git.InitSimple for runRepoSyncReleases (#26396) (#26450)
* Add transaction when creating pull request created dirty data (#26259) (#26437)
* Fix wrong middleware sequence (#26428) (#26436)
* Fix admin queue page title and fix CI failures (#26409) (#26421)
* Introduce ctx.PathParamRaw to avoid incorrect unescaping (#26392) (#26405)
* Bypass MariaDB performance bug of the "IN" sub-query, fix incorrect IssueIndex (#26279) (#26368)
* Fix incorrect CLI exit code and duplicate error message (#26346) (#26347)
* Prevent newline errors with Debian packages (#26332) (#26342)
* Fix bug with sqlite load read (#26305) (#26339)
* Make git batch operations use parent context timeout instead of default timeout (#26325) (#26330)
* Support getting changed files when commit ID is EmptySHA (#26290) (#26316)
* Clarify the logger's MODE config option (#26267) (#26281)
* Use shared template for webhook icons (#26242) (#26246)
* Fix pull request check list is limited (#26179) (#26245)
* Fix attachment clipboard copy on insecure origin (#26224) (#26231)
* Fix access check for org-level project (#26182) (#26223)
* Improve profile readme rendering (#25988) (#26453)
* [docs] Add missing backtick in quickstart.zh-cn.md (#26349) (#26357)
* Upgrade x/net to 0.13.0 (#26301)

[1.29.4]
* Update Gitea to 1.20.4
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.20.4)
* Check blocklist for emails when adding them to account (#26812) (#26831)
* Add branch_filter to hooks API endpoints (#26599) (#26632)
* Fix incorrect "tabindex" attributes (#26733) (#26734)
* Use line-height: normal by default (#26635) (#26708)
* Fix unable to display individual-level project (#26198) (#26636)
* Fix wrong review requested number (#26784) (#26880)
* Avoid double-unescaping of form value (#26853) (#26863)
* Redirect from {repo}/issues/new to {repo}/issues/new/choose when blank issues are disabled (#26813) (#26847)
* Sync tags when adopting repos (#26816) (#26834)
* Fix verifyCommits error when push a new branch (#26664) (#26810)
* Include the GITHUB_TOKEN/GITEA_TOKEN secret for fork pull requests (#26759) (#26806)
* Fix some slice append usages (#26778) (#26798)
* Add fix incorrect can_create_org_repo for org owner team (#26683) (#26791)
* Fix bug for ctx usage (#26763)
* Make issue template field template access correct template data (#26698) (#26709)
* Use correct minio error (#26634) (#26639)
* Ignore the trailing slashes when comparing oauth2 redirect_uri (#26597) (#26618)
* Set errwriter for urfave/cli v1 (#26616)
* Fix reopen logic for agit flow pull request (#26399) (#26613)
* Fix context filter has no effect in dashboard (#26695) (#26811)
* Fix being unable to use a repo that prohibits accepting PRs as a PR source. (#26785) (#26790)
* Fix Page Not Found error (#26768)

[1.30.0]
* Implement OIDC auth

[1.30.1]
* Update Gitea to 1.20.5
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.20.5)
* Fix z-index on markdown completion (#27237) (#27242 & #27238)
* Use secure cookie for HTTPS sites (#26999) (#27013)
* Fix git 2.11 error when checking IsEmpty (#27393) (#27396)
* Allow get release download files and lfs files with oauth2 token format (#26430) (#27378)
* Fix orphan check for deleted branch (#27310) (#27320)
* Quote table release in sql queries (#27205) (#27219)
* Fix release URL in webhooks (#27182) (#27184)
* Fix successful return value for SyncAndGetUserSpecificDiff (#27152) (#27156)
* fix pagination for followers and following (#27127) (#27138)
* Fix issue templates when blank isses are disabled (#27061) (#27082)
* Fix context cache bug & enable context cache for dashabord commits' authors(#26991) (#27017)
* Fix INI parsing for value with trailing slash (#26995) (#27001)
* Fix PushEvent NullPointerException jenkinsci/github-plugin (#27203) (#27249)
* Fix organization field being null in POST /orgs/{orgid}/teams (#27150) (#27167 & #27162)
* Fix bug of review request number (#27406) (#27104)

[1.31.0]
* Update base image to 4.2.0

[1.32.0]
* Update Gitea to 1.21.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.0)

[1.32.1]
* Update Gitea to 1.21.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.1)

[1.32.2]
* Update Gitea to 1.21.2
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.2)
* Rebuild with recently released golang version
* Fix missing check (#28406) (#28411)
* Do some missing checks (#28423) (#28432)
* Fix margin in server signed signature verification view (#28379) (#28381)
* Fix object does not exist error when checking citation file (#28314) (#28369)
* Use filepath instead of path to create SQLite3 database file (#28374) (#28378)
* Fix the runs will not be displayed bug when the main branch have no workflows but other branches have (#28359) (#28365)
* Handle repository.size column being NULL in migration v263 (#28336) (#28363)
* Convert git commit summary to valid UTF8. (#28356) (#28358)
* Fix migration panic due to an empty review comment diff (#28334) (#28362)
* Add HEAD support for rpm repo files (#28309) (#28360)
* Fix RPM/Debian signature key creation (#28352) (#28353)
* Keep profile tab when clicking on Language (#28320) (#28331)
* Fix missing issue search index update when changing status (#28325) (#28330)
* Fix wrong link in protect_branch_name_pattern_desc (#28313) (#28315)
* Read previous info from git blame (#28306) (#28310)
* Ignore "non-existing" errors when getDirectorySize calculates the size (#28276) (#28285)
* Use appSubUrl for OAuth2 callback URL tip (#28266) (#28275)
* Meilisearch: require all query terms to be matched (#28293) (#28296)
* Fix required error for token name (#28267) (#28284)
* Fix issue will be detected as pull request when checking First-time contributor (#28237) (#28271)
* Use full width for project boards (#28225) (#28245)
* Increase "version" when update the setting value to a same value as before (#28243) (#28244)
* Also sync DB branches on push if necessary (#28361) (#28403)
* Make gogit Repository.GetBranchNames consistent (#28348) (#28386)
* Recover from panic in cron task (#28409) (#28425)
* Deprecate query string auth tokens (#28390) (#28430)
* Improve doctor cli behavior (#28422) (#28424)
* Fix margin in server signed signature verification view (#28379) (#28381)
* Refactor template empty checks (#28351) (#28354)
* Read previous info from git blame (#28306) (#28310)
* Use full width for project boards (#28225) (#28245)
* Enable system users search via the API (#28013) (#28018)

[1.32.3]
* Update Gitea to 1.21.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.3)
* Update golang.org/x/crypto (#28519)
* chore(api): support ignore password if login source type is LDAP for creating user API (#28491) (#28525)
* Add endpoint for not implemented Docker auth (#28457) (#28462)
* Add option to disable ambiguous unicode characters detection (#28454) (#28499)
* Refactor SSH clone URL generation code (#28421) (#28480)
* Polyfill SubmitEvent for PaleMoon (#28441) (#28478)
* Fix the issue ref rendering for wiki (#28556) (#28559)
* Fix duplicate ID when deleting repo (#28520) (#28528)
* Only check online runner when detecting matching runners in workflows (#28286) (#28512)
* Initalize stroage for orphaned repository doctor (#28487) (#28490)
* Fix possible nil pointer access (#28428) (#28440)
* Don't show unnecessary citation JS error on UI (#28433) (#28437)
* Fix inperformant query on retrifing review from database. (#28552) (#28562)
* Improve the prompt for "ssh-keygen sign" (#28509) (#28510)
* Update docs for DISABLE_QUERY_AUTH_TOKEN (#28485) (#28488)
* Fix Chinese translation of config cheat sheet[API] (#28472) (#28473)
* Retry SSH key verification with additional CRLF if it failed (#28392) (#28464)

[1.32.4]
* Update Gitea to 1.21.4
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.4)
* Update github.com/cloudflare/circl (#28789) (#28790)
* Require token for GET subscription endpoint (#28765) (#28768)
* Use refname:strip-2 instead of refname:short when syncing tags (#28797) (#28811)
* Fix links in issue card (#28806) (#28807)
* Fix nil pointer panic when exec some gitea cli command (#28791) (#28795)
* Require token for GET subscription endpoint (#28765) (#28778)
* Fix button size in "attached header right" (#28770) (#28774)
* Fix convert.ToTeams on empty input (#28426) (#28767)
* Hide code related setting options in repository when code unit is disabled (#28631) (#28749)

[1.32.5]
* Update Gitea to 1.21.5
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.5)
* Prevent anonymous container access if RequireSignInView is enabled (#28877) (#28882)
* Update go dependencies and fix go-git (#28893) (#28934)
* Revert "Speed up loading the dashboard on mysql/mariadb (#28546)" (#29006) (#29007)
* Fix an actions schedule bug (#28942) (#28999)
* Fix update enable_prune even if mirror_interval is not provided (#28905) (#28929)
* Fix uploaded artifacts should be overwritten (#28726) backport v1.21 (#28832)
* Preserve BOM in web editor (#28935) (#28959)
* Strip / from relative links (#28932) (#28952)
* Don't remove all mirror repository's releases when mirroring (#28817) (#28939)
* Implement MigrateRepository for the actions notifier (#28920) (#28923)
* Respect branch info for relative links (#28909) (#28922)
* Don't reload timeline page when (un)resolving or replying conversation (#28654) (#28917)
* Only migrate the first 255 chars of a Github issue title (#28902) (#28912)
* Fix sort bug on repository issues list (#28897) (#28901)
* Fix DeleteCollaboration transaction behaviour (#28886) (#28889)
* Fix schedule not trigger bug because matching full ref name with short ref name (#28874) (#28888)
* Fix migrate storage bug (#28830) (#28867)
* Fix archive creating LFS hooks and breaking pull requests (#28848) (#28851)
* Fix reverting a merge commit failing (#28794) (#28825)
* Upgrade xorm to v1.3.7 to fix a resource leak problem caused by Iterate (#28891) (#28895)
* Fix incorrect PostgreSQL connection string for Unix sockets (#28865) (#28870)

[1.32.6]
* Update Gitea to 1.21.7
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.7)
* Fix XSS vulnerabilities (#29336)
* Use general token signing secret (#29205) (#29325)
* Refactor issue template parsing and fix API endpoint (#29069) (#29140)
* Fix swift packages not resolving (#29095) (#29102)
* Refactor git version functions and check compatibility (#29155) (#29157)
* Improve user experience for outdated comments (#29050) (#29086)
* Hide code links on release page if user cannot read code (#29064) (#29066)
* Wrap contained tags and branches again (#29021) (#29026)
* Fix incorrect button CSS usages (#29015) (#29023)
* Strip trailing newline in markdown code copy (#29019) (#29022)
* Remove SSH workaround (#27893) (#29332)
* Only log error when tag sync fails (#29295) (#29327)
* Fix SSPI user creation (#28948) (#29323)
* Improve the issue_comment workflow trigger event (#29277) (#29322)
* Discard unread data of git cat-file (#29297) (#29310)
* Fix error display when merging PRs (#29288) (#29309)
* Prevent double use of git cat-file session. (#29298) (#29301)
* Fix missing link on outgoing new release notifications (#29079) (#29300)
* Fix debian InRelease Acquire-By-Hash newline (#29204) (#29299)
* Always write proc-receive hook for all git versions (#29287) (#29291)
* Do not show delete button when time tracker is disabled (#29257) (#29279)
* Workaround to clean up old reviews on creating a new one (#28554) (#29264)
* Fix bug when the linked account was disactived and list the linked accounts (#29263)
* Do not use lower tag names to find releases/tags (#29261) (#29262)
* Fix missed edit issues event for actions (#29237) (#29251)
* Only delete scheduled workflows when needed (#29091) (#29235)
* Make submit event code work with both jQuery event and native event (#29223) (#29234)
* Fix push to create with capitalize repo name (#29090) (#29206)
* Use ghost user if user was not found (#29161) (#29169)
* Dont load Review if Comment is CommentTypeReviewRequest (#28551) (#29160)
* Refactor parseSignatureFromCommitLine (#29054) (#29108)
* Avoid showing unnecessary JS errors when there are elements with different origin on the page (#29081) (#29089)
* Fix gitea-origin-url with default ports (#29085) (#29088)
* Fix orgmode link resolving (#29024) (#29076)
* Fix: Elasticsearch: Request Entity Too Large #28117 (#29062) (#29075)
* Do not render empty comments (#29039) (#29049)
* Avoid sending update/delete release notice when it is draft (#29008) (#29025)

[1.32.7]
* Update Gitea to 1.21.8
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.8)
* Only use supported sort orders for "/explore/users" page (#29430) (#29443)
* Fix wrong line number in code search result (#29260) (#29623)
* Use Get but not Post to get actions artifacts (#29734) (#29737)
* Fix inconsistent rendering of block mathematical expressions (#29677) (#29711)
* Fix rendering internal file links in org (#29669) (#29705)
* Don't show AbortErrors on logout (#29639) (#29667)
* Fix user-defined markup links targets (#29305) (#29666)
* Fix incorrect rendering csv file when file size is larger than UI.CSV.MaxFileSize (#29653) (#29663)
* Fix hidden test's failure (#29254) (#29662)

[1.32.8]
* Update Gitea to 1.21.9
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.9)
* Only do counting when count_only=true for repo dashboard (#29884) (#29905)
* Add cache for dashboard commit status (#29932)
* Make runs-on support variable expression (#29468) (#29782)
* Show Actions post step when it's running (#29926) (#29928)
* Fix PR creation via API between branches of the same repo with head field namespaced (#26986) (#29857)
* Fix and rewrite markup anchor processing (#29931) (#29946)
* Notify reviewers added via CODEOWNERS (#29842) (#29902)
* Fix template error when comment review doesn't exist (#29888) (#29889)
* Fix user id column case (#29863) (#29867)
* Make meilisearch do exact search for issues (#29740 & #29671) (#29846)
* Fix the for attribute not pointing to the ID of the color picker (#29813) (#29815)
* Fix codeowner detected diff base branch to mergebase (#29783) (#29807)
* Fix Safari spinner rendering (#29801) (#29802)
* Fix missing translation on milestones (#29785) (#29789)
* Fix user router possible panic (#29751) (#29786)
* Fix possible NPE in ToPullReviewList (#29759) (#29775)
* Fix the wrong default value of ENABLE_OPENID_SIGNIN on docs (#29925) (#29927)
* Solving the issue of UI disruption when the review is deleted without refreshing (#29951) (#29968)
* Fix loadOneBranch panic (#29938) (#29939)
* Fix invalid link of the commit status when ref is tagged (#29752) (#29908)
* Editor error message misleading due to re-used key. (#29859) (#29876)
* Fix double border and border-radius on empty action steps (#29845) (#29850)
* Use Temporal.PlainDate for absolute dates (#29804) (#29808)
* Fix incorrect package link method calls in templates (#29580) (#29764)
* Fix the bug that the user may log out if GetUserByID returns unknown error (#29962) (#29964)
* Performance improvements for pull request list page (#29900) (#29972)
* Fix bugs in rerunning jobs (#29983) (#29955)

[1.32.9]
* Update Gitea to 1.21.10
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.10)
* Fix Add/Remove WIP on pull request title failure (#29999) (#30066)
* Fix misuse of TxContext (#30061) (#30062)
* Respect DEFAULT_ORG_MEMBER_VISIBLE setting when adding creator to org (#30013) (#30035)
* Escape paths for find file correctly (#30026) (#30031)
* Remove duplicate option in admin screen and now-unused translation keys (#28492) (#30024)
* Fix manual merge form and 404 page templates (#30000)

[1.32.10]
* Update Gitea to 1.21.11
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.21.11)
* Use go1.21.9 to include Golang security fix
* Fix possible renderer security problem (#30136) (#30315)
* Fix close file in the Upload func (#30262) (#30269)
* Fix inline math blocks can't be preceeded/followed by alphanumerical characters (#30175) (#30250)
* Fix missing 0 prefix of GPG key id (#30245) (#30247)
* Include encoding in signature payload (#30174) (#30181)
* Move from max( id ) to max( index ) for latest commit statuses (#30076) (#30155)
* Load attachments for code comments (#30124) (#30126)
* Fix gitea doctor will remove repo-avatar files when executing command storage-archives (#30094) (#30120)
* Fix possible data race on tests (#30093) (#30108)
* Performance optimization for git push (#30104)
* Fix duplicate migrated milestones (#30102) (#30105)
* Fix panic for fixBrokenRepoUnits16961 (#30068) (#30100)
* Fix incorrect SVGs (#30087)
* Fix create commit status (#30225) (#30340)
* Performance optimization for git push (#30104) (#30354)
* Fix misuse of unsupported global variables (#30402)
* Fix to delete the cookie when AppSubURL is non-empty (#30375) (#30468)
* Avoid user does not exist error when detecting schedule actions when the commit author is an external user (#30357) (#30408)
* Change the default maxPerPage for gitbucket (#30392) (#30471)
* Check the token's owner and repository when registering a runner (#30406) (#30412)
* Avoid losing token when updating mirror settings (#30429) (#30466)
* Fix commit status cache which missed target_url (#30426) (#30445)
* Fix rename branch 500 when the target branch is deleted but exist in database (#30430) (#30437)
* Fix mirror error when mirror repo is empty (#30432) (#30467)
* Use db.ListOptions directly instead of Paginator interface to make it easier to use and fix performance of /pulls and /issues (#29990) (#30447)
* Fix code owners will not be mentioned when a pull request comes from a forked repository (#30476) (#30497)

[1.33.0]
* Update Gitea to 1.22.0
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.22.0)
* [Breaking changes](https://github.com/go-gitea/gitea/releases/tag/v1.22.0#Breaking)
* Allow everyone to read or write a wiki by a repo unit setting (#30495)
* Use raw Wiki links for non-renderable Wiki files (#30273)
* Render embedded code preview by permalink in markdown (#30234) (#30249)
* Support repo code search without setting up an indexer (#29998)
* Support pasting URLs over markdown text (#29566)
* Allow to change primary email before account activation (#29412)
* Customizable "Open with" applications for repository clone (#29320)
* Allow options to disable user deletion from the interface on app.ini (#29275)
* Extend issue template YAML engine (#29274)

[1.33.1]
* Update Gitea to 1.22.1
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.22.1)

[1.33.2]
* Update Gitea to 1.22.2
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.22.2)
* Replace v-html with v-text in search inputbox (#31966) (#31973)
* Fix nuget/conan/container packages upload bugs (#31967) (#31982)
* Refactor the usage of batch catfile (#31754) (#31889)
* Fix overflowing content in action run log (#31842) (#31853)
* Scroll images in project issues separately from the remaining issue (#31683) (#31823)
* Add :focus-visible style to buttons (#31799) (#31819)
* Fix the display of project type for deleted projects (#31732) (#31734)
* Fix API owner ID should be zero when created repo secret (#31715) (#31811)
* Set owner id to zero when GetRegistrationToken for repo (#31725) (#31729)
* Fix API endpoint for registration-token (#31722) (#31728)
* Add permission check when creating PR (#31033) (#31720)

[1.33.3]
* Update Gitea to 1.22.3
* [Full changelog](https://github.com/go-gitea/gitea/releases/tag/v1.22.3)
* Fix bug when a token is given public only (#32204) (#32218)
* Increase cacheContextLifetime to reduce false reports (#32011) (#32023)
* Don't join repository when loading action table data (#32127) (#32143)
* Fix javascript error when an anonymous user visits migration page (#32144) (#32179)
* Don't init signing keys if oauth2 provider is disabled (#32177)
* Fix wrong status of Set up Job when first step is skipped (#32120) (#32125)
* Fix bug when deleting a migrated branch (#32075) (#32123)
* Truncate commit message during Discord webhook push events (#31970) (#32121)
* Allow to set branch protection in an empty repository (#32095) (#32119)
* Fix panic when cloning with wrong ssh format. (#32076) (#32118)
[1.33.4]
* Update gitea to 1.22.4
* [Full Changelog](https://github.com/go-gitea/gitea/releases/tag/v1.22.4)
* Fix basic auth with webauthn (#32531) (#32536)
* Refactor internal routers (partial backport, auth token const time comparing) (#32473) (#32479)

[1.33.5]
* Update gitea to 1.22.5
* [Full Changelog](https://github.com/go-gitea/gitea/releases/tag/v1.22.5)
* Upgrade crypto library ([#&#8203;32791](https://github.com/go-gitea/gitea/issues/32791))
* Fix delete branch perm checking ([#&#8203;32654](https://github.com/go-gitea/gitea/issues/32654)) ([#&#8203;32707](https://github.com/go-gitea/gitea/issues/32707))
* Add standard-compliant route to serve outdated R packages ([#&#8203;32783](https://github.com/go-gitea/gitea/issues/32783)) ([#&#8203;32789](https://github.com/go-gitea/gitea/issues/32789))
* Fix internal server error when updating labels without write permission ([#&#8203;32776](https://github.com/go-gitea/gitea/issues/32776)) ([#&#8203;32785](https://github.com/go-gitea/gitea/issues/32785))
* Add Swift login endpoint ([#&#8203;32693](https://github.com/go-gitea/gitea/issues/32693)) ([#&#8203;32701](https://github.com/go-gitea/gitea/issues/32701))
* Fix fork page branch selection ([#&#8203;32711](https://github.com/go-gitea/gitea/issues/32711)) ([#&#8203;32725](https://github.com/go-gitea/gitea/issues/32725))
* Fix word overflow in file search page ([#&#8203;32695](https://github.com/go-gitea/gitea/issues/32695)) ([#&#8203;32699](https://github.com/go-gitea/gitea/issues/32699))
* Fix gogit `GetRefCommitID` ([#&#8203;32705](https://github.com/go-gitea/gitea/issues/32705)) ([#&#8203;32712](https://github.com/go-gitea/gitea/issues/32712))
* Fix race condition in mermaid observer ([#&#8203;32599](https://github.com/go-gitea/gitea/issues/32599)) ([#&#8203;32673](https://github.com/go-gitea/gitea/issues/32673))
* Fixe a keystring misuse and refactor duplicates keystrings ([#&#8203;32668](https://github.com/go-gitea/gitea/issues/32668)) ([#&#8203;32792](https://github.com/go-gitea/gitea/issues/32792))
* Bump relative-time-element to v4.4.4 ([#&#8203;32739](https://github.com/go-gitea/gitea/issues/32739))
* Make wiki pages visit fast ([#&#8203;32732](https://github.com/go-gitea/gitea/issues/32732)) ([#&#8203;32745](https://github.com/go-gitea/gitea/issues/32745))
* Don't create action when syncing mirror pull refs ([#&#8203;32659](https://github.com/go-gitea/gitea/issues/32659)) ([#&#8203;32664](https://github.com/go-gitea/gitea/issues/32664))

[1.33.6]
* Update gitea to 1.22.6
* [Full Changelog](https://github.com/go-gitea/gitea/releases/tag/v1.22.6)
* Fix misuse of PublicKeyCallback([#&#8203;32810](https://github.com/go-gitea/gitea/issues/32810))
* Fix lfs migration ([#&#8203;32812](https://github.com/go-gitea/gitea/issues/32812)) ([#&#8203;32818](https://github.com/go-gitea/gitea/issues/32818))
* Add missing two sync feed for refs/pull ([#&#8203;32815](https://github.com/go-gitea/gitea/issues/32815))
* Avoid MacOS keychain dialog in integration tests ([#&#8203;32813](https://github.com/go-gitea/gitea/issues/32813)) ([#&#8203;32816](https://github.com/go-gitea/gitea/issues/32816))

[1.34.0]
* Update gitea to 1.23.0
* [Full Changelog](https://github.com/go-gitea/gitea/releases/tag/v1.23.0)
* Rename config option `[camo].Allways` to `[camo].Always` ([#&#8203;32097](https://github.com/go-gitea/gitea/issues/32097))
* Remove SHA1 for support for SSH RSA signing ([#&#8203;31857](https://github.com/go-gitea/gitea/issues/31857))
* Use UTC as the default timezone when scheduling Actions cron tasks ([#&#8203;31742](https://github.com/go-gitea/gitea/issues/31742))
* Delete Actions logs older than 1 year by default ([#&#8203;31735](https://github.com/go-gitea/gitea/issues/31735))
* Make OIDC introspection authentication strictly require Client ID and secret ([#&#8203;31632](https://github.com/go-gitea/gitea/issues/31632))
* Include file extension checks in attachment API ([#&#8203;32151](https://github.com/go-gitea/gitea/issues/32151))

[1.34.1]
* Update gitea to 1.23.1
* [Full Changelog](https://github.com/go-gitea/gitea/releases/tag/v1.23.1)
* Move repo size to sidebar ([#&#8203;33155](https://github.com/go-gitea/gitea/issues/33155))
* Fix editor markdown not incrementing in a numbered list ([#&#8203;33187](https://github.com/go-gitea/gitea/issues/33187)) [#&#8203;33193](https://github.com/go-gitea/gitea/issues/33193)

[1.34.2]
* Update gitea to 1.23.3
* [Full Changelog](https://github.com/go-gitea/gitea/releases/tag/v1.23.3)
* Build Gitea with Golang v1.23.6 to fix security bugs
* Fix a bug caused by status webhook template #33512

[1.34.3]
* Update gitea to 1.23.4
* [Full Changelog](https://github.com/go-gitea/gitea/releases/tag/v1.23.4)
* Enhance routers for the Actions variable operations (#33547) (#33553)
* Enhance routers for the Actions runner operations (#33549) (#33555)
* Fix project issues list and counting (#33594) #33619
* Add a transaction to pickTask (#33543) (#33563)
* Fix mirror bug (#33597) (#33607)
* Use default Git timeout when checking repo health (#33593) (#33598)
* Fix PR's target branch dropdown (#33589) (#33591)
* Fix various problems (artifact order, api empty slice, assignee check, fuzzy prompt, mirror proxy, adopt git) (#33569) (#33577)
* Rework suggestion backend (#33538) (#33546)
* Fix context usage (#33554) (#33557)

[1.34.4]
* Update gitea to 1.23.5
* [Full Changelog](https://github.com/go-gitea/gitea/releases/tag/v1.23.5)
* Compile with Go 1.24.1
* Bump x/oauth2 & x/crypto (#33704) (#33727)
* Optimize user dashboard loading (#33686) (#33708)
* Fix navbar dropdown item align (#33782)
* Fix inconsistent closed issue list icon (#33722) (#33728)
* Fix for Maven Package Naming Convention Handling (#33678) (#33679)
* Improve Open-with URL encoding (#33666) (#33680)
* Deleting repository should unlink all related packages (#33653) (#33673)
* Fix omitempty bug (#33663) (#33670)
* Upgrade go-crypto from 1.1.4 to 1.1.6 (#33745) (#33754)
* Fix OCI image.version annotation for releases to use full semver (#33698) (#33701)
* Try to fix ACME path when renew (#33668) (#33693)
* Fix mCaptcha bug (#33659) (#33661)
* Git graph: don't show detached commits (#33645) (#33650)

[1.35.0]
* Base image 5

