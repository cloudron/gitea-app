FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN apt-get update && \
    apt-get install -y openssh-server git asciidoctor pandoc pipx && \
    rm -rf /etc/ssh_host_* && \
    rm -r /var/cache/apt /var/lib/apt/lists

# pipx --global does not work, not sure why
RUN PIPX_HOME=/opt/pipx PIPX_BIN_DIR=/usr/local/bin pipx install jupyter docutils --include-deps

ADD supervisor/ /etc/supervisor/conf.d/

RUN useradd --comment "Gogs" --create-home --shell /bin/bash git
RUN passwd -d git # by default, git account is created as inactive which prevents login via openssh. this disables password for account
WORKDIR /home/git

# for autosign feature
ENV GNUPGHOME="/app/data/gnupg"

# renovate: datasource=github-releases depName=go-gitea/gitea versioning=semver extractVersion=^v(?<version>.+)$
ARG GITEA_VERSION=1.23.5

RUN mkdir -p /home/git/gitea && \
    curl -L https://github.com/go-gitea/gitea/releases/download/v${GITEA_VERSION}/gitea-${GITEA_VERSION}-linux-amd64 -o /home/git/gitea/gitea \
    && chmod +x /home/git/gitea/gitea

# setup config paths
ADD app.ini.template /home/git/app.ini.template

# setup log paths
RUN mkdir -p /run/gitea && chown -R git:git /run/gitea
RUN sed -e 's,^logfile=.*$,logfile=/run/gitea/supervisord.log,' -i /etc/supervisor/supervisord.conf

RUN ln -s /app/data/ssh /home/git/.ssh
RUN ln -s /app/data/gitconfig /home/git/.gitconfig

ADD start.sh /home/git/start.sh

COPY sshd_config /etc/ssh/sshd_config

CMD [ "/home/git/start.sh" ]

