This app is pre-setup with an admin account. The initial credentials are:

**Username**: root<br/>
**Password**: changeme<br/>

<sso>
Use the `Local` authentication source for logging in as admin.
</sso>
